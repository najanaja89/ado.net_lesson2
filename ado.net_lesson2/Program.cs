﻿using ado.net_lesson2.DataAccess;
using ado.net_lesson2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson2.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataService = new UsersTableDataService();
            dataService.AddUser(new User
            {
                Login = "admin",
                Password = "root"
            });

            foreach (var user in dataService.GetAllUsers())
            {
                System.Console.WriteLine($"{user.Id}, {user.Login}, {user.Password}");
            }

            System.Console.ReadLine();

        }
    }
}
