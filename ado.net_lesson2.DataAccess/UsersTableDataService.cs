﻿using ado.net_lesson2.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Configuration;

namespace ado.net_lesson2.DataAccess
{
    public class UsersTableDataService
    {
        private readonly string _connectionString;
        private readonly string _providerName;
        private readonly DbProviderFactory _providerFactory;

        public UsersTableDataService()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString;
            _providerName = ConfigurationManager.ConnectionStrings["testConnectionString"].ProviderName;
            _providerFactory = DbProviderFactories.GetFactory(_providerName);
        }

        public List<User> GetAllUsers()
        {
            string isAdminValue = ConfigurationManager.AppSettings["isAdmin"];
            if (bool.TryParse(isAdminValue, out var isAdmin) && isAdmin)
            {
                Console.WriteLine("***Administrator Mode is On***");
            }

            var data = new List<User>();

            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                DbTransaction transaction = null;
                try
                {
                    connection.Open();
                    transaction = connection.BeginTransaction();

                    command.CommandText = "select * from users";
                    command.Transaction = transaction;
                    
                    var dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        var id = (int)dataReader["Id"];
                        var login = dataReader["Login"].ToString();
                        var password = dataReader["Password"].ToString();

                        data.Add(new User
                        {
                            Id = id,
                            Login = login,
                            Password = password
                        });
                    }

                    transaction.Commit();
                    transaction.Dispose();
                    dataReader.Close();
                }
                catch (DbException exception)
                {
                    transaction?.Rollback();
                    transaction.Dispose();
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    transaction?.Rollback();
                    transaction.Dispose();
                    //TODO обработка ошибки
                    throw;
                }
            }
            return data;
        }

        public void AddUser(User user)
        {
            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                DbTransaction transaction = null;
                try
                {
                    connection.Open();
                    command.CommandText = $"insert into Users values(@login, @password)";
                    command.Transaction = transaction;

                    DbParameter loginParameter = command.CreateParameter();
                    loginParameter.ParameterName = "@login";
                    loginParameter.Value = user.Login;
                    loginParameter.DbType = System.Data.DbType.String;
                    loginParameter.IsNullable = false;


                    DbParameter passwordParameter = command.CreateParameter();
                    passwordParameter.ParameterName = "@password";
                    passwordParameter.Value = user.Password;
                    passwordParameter.DbType = System.Data.DbType.String;
                    passwordParameter.IsNullable = false;


                    command.Parameters.AddRange(new DbParameter[] { loginParameter, passwordParameter });

                    var affectedRows = command.ExecuteNonQuery();

                    if (affectedRows < 1)
                    {
                        throw new Exception("Insert not done");
                    }
                    transaction.Commit();
                    transaction.Dispose();
                }
                catch (DbException exception)
                {

                    //TODO обработка ошибки
                    transaction?.Rollback();
                    transaction.Dispose();
                    throw;
                }
                catch (Exception exception)
                {
                    transaction?.Rollback();
                    transaction.Dispose();
                    //TODO обработка ошибки
                    throw;
                }
            }
        }

        public void DeleteUserById(int id)
        {

        }

        public void UpdateUser(User user)
        {

        }
    }
}
